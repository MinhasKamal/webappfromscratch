import * as Express from 'express';
import * as bodyParser from 'body-parser';
import {RouteHandler} from './utils/routeHandler';

let server = Express();
server.post('/', RouteHandler.handleRootRoute);
server.post('/number', RouteHandler.handleNumberRoute);
server.post('/string', RouteHandler.handleStringRoute);
server.post('/data', bodyParser.json(), RouteHandler.handleDataRoute);
server.post('/data/add', bodyParser.json(), RouteHandler.handleDataAddRoute);
server.post('/data/edit', bodyParser.json(), RouteHandler.handleDataEditRoute);
server.post('/data/delete', bodyParser.json(), RouteHandler.handleDataDeleteRoute);
server.post('/data/search', bodyParser.json(), RouteHandler.handleDataSearchRoute);
server.post('/*', RouteHandler.handleRootRoute);
let port: number = 55;
server.listen(port, serverStartCallback);
function serverStartCallback (): void {
    console.log("server started!");
}
