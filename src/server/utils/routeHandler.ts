import {Request, Response} from 'express';
import {Random} from './random';
import {DBHandler} from './dBHandler';

export class RouteHandler {

    private static random: Random = new Random();
    private static dBHandler: DBHandler = new DBHandler();

    public static handleRootRoute (req: Request, res: Response): void {
        RouteHandler.handleNumberRoute(req, res);
    }

    public static handleNumberRoute (req: Request, res: Response): void {
        let num: number = RouteHandler.random.nextInteger(100);
        res.send("Number: " + num);
    }

    public static handleStringRoute (req: Request, res: Response): void {
        res.send("String: loerem epsum");
    }

    public static handleDataAddRoute (req: Request, res: Response): void {
        RouteHandler.dBHandler.insertData(req.body.title, req.body.content, res);
    }

    public static handleDataEditRoute (req: Request, res: Response): void {
        RouteHandler.dBHandler.editData(req.body.id, req.body.title, req.body.content, res);
    }

    public static handleDataDeleteRoute (req: Request, res: Response): void {
        RouteHandler.dBHandler.deleteData(req.body.title, res);
    }

    public static handleDataSearchRoute (req: Request, res: Response): void {
        RouteHandler.dBHandler.searchData(req.body.text, res);
    }

    public static handleDataRoute (req: Request, res: Response): void {
        RouteHandler.dBHandler.getData(res);
    }
}
