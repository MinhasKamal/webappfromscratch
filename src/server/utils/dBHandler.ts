import {Response} from 'express';
import * as mongoose from "mongoose";
import {INotice, NoticeSchema} from './../models/notice';

export class DBHandler {
    private dbUri: string = "mongodb://localhost:27017/noticeBoard";

    public constructor() {
        mongoose.connect(this.dbUri, DBHandler.dbServerStartCallback);
    }

    private static dbServerStartCallback(err: object): void {
        if (err) {
            console.log(err);
        } else {
            console.log("connected to mongoDb!");
        }
    }

    public searchData(text: string, res: Response): void {
        let notices = mongoose.model("notices", NoticeSchema);
        notices.find({$text: {$search: text}}).exec(function(err, noticeList) {
            if(err){
                res.send(err);
                console.log(err);
            }else {
                res.send(noticeList);
                console.log('searched');
            }
        });
    }

    public getData(res: Response): void {
        let notices = mongoose.model("notices", NoticeSchema);
        notices.find(function(err, notice) {
            if(err){
                res.send(err);
                console.log(err);
            }else {
                res.send(notice);
            }
        });
    }

    public async insertData(title: string, content: string, res: Response){
        let notices = mongoose.model("notices", NoticeSchema);
        let sequence = await this.getNextSequence();
        notices.insertMany([{title: title, content: content, sequence: sequence}], function(err){
            if(err){
                res.send(err);
                console.log(err);
            }else {
                res.send("inserted");
            }
        });
    }

    private async getNextSequence(): Promise<number>{
        let notices = mongoose.model("notices", NoticeSchema);
        let noticeSeq: number = 0;
        await notices.find(function(err, noticeList: INotice[]) {
            if(err){
                console.log(err);
            }else {
                for(let notice of noticeList){
                    if(noticeSeq < notice.sequence){
                        noticeSeq = notice.sequence;
                    }
                }
            }
        });
        return noticeSeq+1;
    }

    public deleteData(title: string, res: Response): void{
        let notices = mongoose.model("notices", NoticeSchema);
        notices.deleteMany({title: title}, function(err){
            if(err){
                res.send(err);
                console.log(err);
            }else {
                res.send("deleted");
            }
        });
    }

    public editData(id: string, title: string, content: string, res: Response): void {
        let notices = mongoose.model("notices", NoticeSchema);

        notices.findByIdAndUpdate(id, {title: title, content: content}, function(err) {
            if(err){
                res.send(err);
                console.log(err);
            }else {
                res.send("edit successful!");
            }
        });
    }
}
