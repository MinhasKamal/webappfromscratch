export class Random {
    private seed: number;
    
    public constructor();
    public constructor(seed?: number) {
        if (seed) {
            this.seed = seed;
        } else {
            this.seed = new Date().getTime();
        }
    }

    public nextInteger(range?: number): number {
        let num: number = 0;

        this.seed = this.seed ^ 59773;
        this.seed = this.seed << 1 | this.seed >> 1;
        num = this.seed << 3 ^ this.seed >> 3;

        if (range) {
            num %= range;
            num = num<0 ? -num:num;
        }
        return num;
    }

}
