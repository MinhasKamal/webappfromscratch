import * as mongoose from "mongoose";

export interface INotice {
    title: string;
    content: string;
    sequence: number;
}

export var NoticeSchema = new mongoose.Schema({
    title: String,
    content: String,
    sequence: Number
});
NoticeSchema.index({'title': 'text', 'content': 'text'});
